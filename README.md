# Aide et ressources de Imagine pour Synchrotron SOLEIL

## Résumé

- traitement exafs / imagerie : extraction NXS+EDF->HDF5 avec alignement, normalisation, extraction EXAFS, TF
- Créé à Synchrotron Soleil

## Sources

- Code source: Oui, mais sur un dépôt 'privé' externe (accès restreint a des personnes identifiées)
- Documentation officielle: docstring dans le code

## Installation

- Systèmes d'exploitation supportés: Linux
- Installation: Facile (tout se passe bien)

## Format de données

- en entrée: NXS+EDF
- en sortie: HDF5, ascii
- sur la Ruche locale